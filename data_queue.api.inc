<?php

/**
 * @file
 * Hooks provided by the Data Queue module.
 */

/**
 * Preprocess file rows.
 * You can alter the file status and override row values.
 */
function hook_data_queue_file_data_preprocess($op, $fieldata, $processed_times) {

  // $fieldata['status']: hold the file read status.
  // $fieldata['data']: hold the file rows.
  // $fieldata['total_rows']: hold the number of all file rows.
  // $fieldata['valid_rows']: hold the number of all valid rows.

  // Check if file rows readable.
  if ($fieldata['status']) {

    // Loop all file rows.
    foreach($fieldata['data'] as $row){
      // ...
      if(qd_validate_xlsx_row($row)){
        // Row data is valid.
      }else{
        // Row data is not valid.
        // @TODO: save it in log.
        $fieldata['valid_rows']--; // By default valid_rows is the same as total_rows.
      }
    }

  }

  return $fieldata;
}

/**
 * Preprocess file rows.
 * $op: - count | process.
 */
function hook_data_queue_file_row_preprocess($op, $row, $totalRows, $validRows) {
  // You can validate row here.
  // Rows counted automatically. So if the row is not valid you can decrease the validRows variable.
  if(dq_validate_xlsx_row($row)){
    $validRows--;
  }
  return array('row'=>$row, 'totalRows'=>$totalRows, 'validRows'=>$validRows);
}

/**
 * Preprocess row.
 * When you make pass FALSE this mean the queue item for this row will not be deleted.
 */
function hook_data_queue_process_row_data($row, $file_data_node) {
  $return_result = array();
  $return_result['pass'] = TRUE;
  return $return_result;
}

/**
 * Validate file rows.
 * $op:
 *   - save: when saving row in queue.
 *   - process: when load row from queue for processing.
 */
function hook_data_queue_validate_row_data($op, $row) {
  $validation_result = array();
  $validation_result['pass'] = TRUE;
  $validation_result['message'] = t('Row is valid.');
  $validation_result['row'] = $row;
  return $validation_result;
}
