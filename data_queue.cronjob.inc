<?php

/**
 * This cron jobs relay on Drupal ultimate_cron contrib module.
 * Make sure ultimate_cron is enabled.
 */

/**
 * Implements hook_cronapi().
 */
function data_queue_cronapi($op, $job = NULL) {
  // This for the non-trigger cron jobs.
  $items['dq_main_cron'] = array(
    'description' => 'Custom Data Queue.',
    'rule' => '* * * * *', // Every minute.
    'callback' => 'dq_cron_handler'
  );
  return $items;
}

/**
 * Implements hook_cron_queue_info().
 */
function data_queue_cron_queue_info() {
  $queues = array();
  $queues['dq_files'] = array(
    'worker callback' => 'dq_files_worker',
    'time' => 30,
    'skip on cron' => TRUE
  );
  $queues['dq_rows'] = array(
    'worker callback' => 'dq_rows_worker',
    'time' => 30,
    'skip on cron' => TRUE
  );
  return $queues;
}

/**
 * Main cron handler will maintain cron interval and prepare custom cron.
 * job queues.
 */
function dq_cron_handler($run_manually = FALSE){

  if(variable_get('data_queue_debug_mode', 0) == 1)
    watchdog('data_queue_debug', 'CRON RAN 1');

  // Default to an hourly interval. Of course, cron has to be running at least
  // hourly for this to work.
  $interval = variable_get('data_queue_cron_interval', 60 * 60);
  // We usually don't want to act every time cron runs (which could be every
  // minute) so keep a time for the next run in a variable.
  if (time() >= variable_get('data_queue_cron_next_execution', 0)) {
    // @TODO: Allow configuration variable to acting like importer user.
    // global $user;
    // $current_user = $user;
    // $user = user_load(2); // 2 = importer user.

    if(variable_get('data_queue_debug_mode', 0) == 1)
      watchdog('data_queue_debug', 'CRON RAN 2');

    // Read and queue needful files.
    dq_files_run();

    // Run imported file rows from queue if there is queue items.
    dq_rows_run();

    // Message to show when run this cron manually.
    if ($run_manually) {
      drupal_set_message(t('Cron executed at %time', array('%time' => date_iso8601(time(0)))));
    }
    variable_set('data_queue_cron_next_execution', time() + $interval);

    // Switch acting like normal user object.
    // $user = $current_user;
  }else{
    if(variable_get('data_queue_debug_mode', 0) == 1)
      watchdog('data_queue_debug', 'CRON RAN 2');
  }

  return TRUE;
}

/**
 * Start reading files.
 */
function dq_files_run(){
  $filesQueue = DrupalQueue::get('dq_files');
  $countThisProcess = 0;
  $thisProcessTotal = variable_get('dq_files_queue_process_files_each_time', 1);
  while ($item = $filesQueue->claimItem(0)) {
    $countThisProcess++;
    if(dq_files_worker($item)){
      $filesQueue->deleteItem($item);
      watchdog('data_queue', t('Queue success for item @item_id for nid: @nid', array('@item_id'=>$item->item_id, '@nid'=>$item->data['nid'])), array(), WATCHDOG_INFO);
    }else{
      //////////////////////////$filesQueue->deleteItem($item);
      $filesQueue->deleteItem($item);
      $filesQueue->createItem($item->data);
      watchdog('data_queue', t('Can not process queueB item @item_id for nid: @nid', array('@item_id'=>$item->item_id, '@nid'=>$item->data['nid'])), array(), WATCHDOG_WARNING);
    }

    // @TODO: We might use release item instead of delete in case of fail.
    if($countThisProcess == $thisProcessTotal){
      break;
    }
  }
}

/**
 * Worker to process file rows and save them in dq_rows queue.
 */
function dq_files_worker($item) {
  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue_debug', 'worker is working on file: '.print_r($item, TRUE).'', array(), WATCHDOG_NOTICE, l(t('File'), 'node/'.$item->data['nid']));

  // Check if file rows has been calculated or not.
  if(isset($item->data['rows']) && ($item->data['rows'] == 0)){
    // Start calculating file rows.
    // This will be processed if this is first time to process this file.

    // field_data_file = dqf_data_file.
    $node = node_load($item->data['nid']);
    if( isset($node->dqf_data_file['und']) ){
      // Reading Excel file.
      $fieldata = dq_read_xlsx_file($node->dqf_data_file['und'][0]['uri']);
      $fieldata['total_rows'] = count($fieldata['data']);
      $fieldata['valid_rows'] = count($fieldata['data']);

      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue_debug', 'Excel file total rows: @total.', array('@total'=>$fieldata['valid_rows']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

      $processed_times = $node->dqf_processed_times[LANGUAGE_NONE][0]['value'];
      // Invoke hooks if there is hooks.
      // $fieldata = module_invoke_all('data_queue_file_data_preprocess', 'count', $fieldata, $processed_times);
      // Give other modules a chance to preprocess file data and alter each other results.
      foreach (module_implements('data_queue_file_data_preprocess') as $module) {
        $function = $module . '_data_queue_file_data_preprocess';
        if (function_exists($function)) {
          // Will call all modules implementing this hook.
          // And pass arguments from previous implement.
          $fieldata = $function('count', $fieldata, $processed_times);
        }
      }

      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue_debug', 'Excel file after hook total rows: @total.', array('@total'=>$fieldata['total_rows']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

      $totalRows = $validRows = 0;
      if($fieldata['status']){
        if(variable_get('data_queue_debug_mode', FALSE))
          watchdog('data_queue_debug', 'Start calculating file rows for nid: @nid.', array('@nid'=>$item->data['nid']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

        // Reading file rows one by one and validate them and get the total number of valid rows.
        foreach($fieldata['data'] as $row){
          // $processed_row = module_invoke_all('data_queue_file_row_preprocess', 'count', $row, $totalRows, $validRows);
          // Give other modules a chance to preprocess row data and alter each other results.
          foreach (module_implements('data_queue_file_row_preprocess') as $module) {
            $function = $module . '_data_queue_file_row_preprocess';
            if (function_exists($function)) {
              // Will call all modules implementing this hook.
              // And pass arguments from previous implement.
              $processed_row = $function('count', $row, $totalRows, $validRows);
              $row = $processed_row['row'];
              $totalRows = $processed_row['totalRows'];
              $validRows = $processed_row['validRows'];
            }
          }

          $row = $processed_row['row'];
          $totalRows = $processed_row['totalRows'];
          $validRows = $processed_row['validRows'];
        }

        // Save row numbers in queue.
        // @TODO: Save validRows or totalRows.
        $item->data['rows'] = $totalRows;

        if(variable_get('data_queue_debug_mode', FALSE))
          watchdog('data_queue_debug', 'Rows has been calculated for nid: @nid with @rows rows (original rows is @original).', array('@nid'=>$item->data['nid'], '@rows'=>$item->data['rows'], '@original'=>count($fieldata['data'])), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

        $worker_array = array();
        $worker_array['total_rows'] = $totalRows;
        $worker_array['valid_rows'] = $validRows;

        // Update node with the new calculated file rows.
        // We will use node_load and node_save to save revision automatically.
        // @TODO: @Check: Make sue the revision is saved here.
        $nodeRows = node_load($node->nid, NULL, TRUE);
        $nodeRows->dqf_rows[LANGUAGE_NONE][0]['value'] = $totalRows;
        $nodeRows->dqf_valid_rows[LANGUAGE_NONE][0]['value'] = $validRows;
        $nodeRows->dqf_status_processed[LANGUAGE_NONE][0]['value'] = 2;
        $nodeRows->dqf_processed_times[LANGUAGE_NONE][0]['value'] = ($nodeRows->dqf_processed_times[LANGUAGE_NONE][0]['value'] + 1);
        $nodeRows->revision = 1; // Enable revision.
        $nodeRows->log = t('Start processing rows.');
        node_save($nodeRows);

        // @TODO: Make sure the node_save saved successfully.
        if(variable_get('data_queue_debug_mode', FALSE))
          watchdog('data_queue', 'Rows number has been updated for nid: @nid with @rows rows.', array('@nid'=>$item->data['nid'], '@rows'=>$item->data['rows']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));
      }
    }

    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue_debug', 'Rows has been calculated for nid: @nid with @rows rows (original rows is @original).', array('@nid'=>$item->data['nid'], '@rows'=>$item->data['rows'], '@original'=>count($fieldata['data'])), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));
  }

  // Start process rows.
  // Calculating start and end rows.
  $from_row = $item->data['queued_rows'];
  $to_row_case1 = $from_row + variable_get('dq_files_queue_insert_rows_each_time', 5);
  if($to_row_case1 > $item->data['rows']){
    $to_row = $item->data['rows'];
  }else{
    $to_row = $to_row_case1;
  }

  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue', 'Start processing file rows from @from to @to for nid: @nid.', array('@from'=>$from_row, '@to'=>$to_row, '@nid'=>$item->data['nid']), WATCHDOG_INFO, 'node/'.$item->data['nid']);

  // Process file rows and save them in rows queue.
  $process_status = dq_save_file_rows_in_queue($item->data['nid'], $from_row, $to_row, $by_current_user = FALSE);
  if($process_status){
    // Success.

    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue', 'File rows has been processed successfully from @from to @to for nid: @nid.', array('@from'=>$from_row, '@to'=>$to_row, '@nid'=>$item->data['nid']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

    // Get needful queue.
    $filesQueue = DrupalQueue::get('dq_files');

    // Check if all rows has been processed we will delete this item from queue since it is done.
    if($to_row >= $item->data['rows']){
      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue', 'File rows for file nid: @nid is finished and will be released from queue.', array('@nid'=>$item->data['nid']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

      // The import process is completed for this file.
      $filesQueue->deleteItem($item);

      // Change file process status to completed.
      $nodeRows = node_load($item->data['nid'], NULL, TRUE);
      $nodeRows->dqf_status_processed[LANGUAGE_NONE][0]['value'] = 1;
      $nodeRows->revision = 1; // Enable revision.
      $nodeRows->log = t('Processing rows has been completed.');
      node_save($nodeRows);
    }else{
      $new_queued_rows = $item->data['queued_rows'] + ($to_row - $from_row);
      $remain_queued_rows = $item->data['rows'] - $new_queued_rows;
      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue', 'File rows for file nid: @nid has remain @number queued rows.', array('@nid'=>$item->data['nid'], '@number'=>$remain_queued_rows), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));

      // We will delete this and create it again with the new details.
      $filesQueue->deleteItem($item);

      // Create new item in queue.
      $new_item = $item->data;
      $new_item['queued_rows'] = $new_queued_rows;
      $filesQueue->createItem($new_item);

      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue', 'File rows for file nid: @nid requeued to process remaining rows.', array('@nid'=>$item->data['nid']), WATCHDOG_INFO, l(t('File'), 'node/'.$item->data['nid']));
    }

    return TRUE;

  }else{
    // Fail.
    // Save log details.
    $theLog = array();
    $theLog['type'] = 'data_queue_file';
    $theLog['operation'] = 'warning';
    $theLog['form_id'] = 'dq_files_worker_'.$item->data['run_number'];
    $theLog['description'] = t('File ID: @id fail process rows from @from to @to.', array('@id'=>$item->data['nid'], '@from'=>$from_row, '@to'=>$to_row));
    $theLog['ref_char'] = 'node';
    $theLog['ref_numeric'] = $item->data['nid'];
    if( isset($item->data['by']) && is_numeric($item->data['by']) )
      $theLog['uid'] = $item->data['by']; // Log this by the action tacker.
    else
      $theLog['uid'] = 2; // 2 = importer user.
    $theLog['info'] = '';
    event_log_insert($theLog);

    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue', 'File ID: @id fail process rows from @from to @to.', array('@id'=>$item->data['nid'], '@from'=>$from_row, '@to'=>$to_row), WATCHDOG_ERROR, 'node/'.$item->data['nid']);

    return FALSE;
  }
}

/**
 * Taking file_data node nid to process file and save rows as queue items.
 *
 * @param     integer nid
 * @return    boolean
 */
function dq_save_file_rows_in_queue($nid, $from_row = 0, $to_row = FALSE, $by_uid = FALSE){
  global $user;
  $node = node_load($nid);

  // Check if this node exists.
  if( !isset($node->nid) ){
    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue', t('Can not load node id @nid.', array('@nid'=>$nid)), array(), WATCHDOG_INFO);

    return FALSE;
  }
  // Check node type.
  if($node->type != 'data_queue_file'){
    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue', t('Can not process node id @nid because it is not a data_queue_file type.', array('@nid'=>$nid)), array(), WATCHDOG_INFO);

    return FALSE;
  }

  if( isset($node->dqf_data_file['und']) ){
    // Reading Excel file.
    // $rowsQueue->numberOfItems();
    $fieldata = dq_read_xlsx_file($node->dqf_data_file['und'][0]['uri']);
    $fieldata['total_rows'] = count($fieldata['data']);
    $fieldata['valid_rows'] = count($fieldata['data']);

    $processed_times = $node->dqf_processed_times[LANGUAGE_NONE][0]['value'];
    //$fieldata = module_invoke_all('data_queue_file_data_preprocess', 'process', $fieldata, $processed_times);

    // $process_operation = 'process';
    // drupal_alter('data_queue_file_data', $fieldata, $process_operation, $processed_times);

    // Give other modules a chance to prepare the file before process and alter each other.
    foreach (module_implements('data_queue_file_data_preprocess') as $module) {
      $function = $module . '_data_queue_file_data_preprocess';
      if (function_exists($function)) {
        // Will call all modules implementing this hook.
        // And pass arguments from previous implement.
        $fieldata = $function('process', $fieldata, $processed_times);
      }
    }

    // @TODO: THIS IF CONDITION SHOULD BE RE-WRITTEN.
    // Case: When the rows is 7 and the status of hook_data_queue_validate_row_data and hook_data_queue_process_row_data.
    // equal FALSE.
    if(($fieldata['status']) && (count($fieldata['data']) > 0)){
      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue', 'Reading file successfully.', array(), WATCHDOG_INFO);

      // Get needful queue.
      $rowsQueue = DrupalQueue::get('dq_rows');

      // Getting rows one by one and save them in import data queue.
      $counted_rows = 0;
      $row_counter = 0; // Counting all rows regardless status.

      // Here we have to ways.
      // 1. Fetching specific rows from to.
      // 2. Fetching all rows.
      // So, we will check if there is selected end or not.
      if(($to_row) && is_numeric($to_row) && ($to_row > 0)){
        // We will select rows.
        $row_counter = $from_row;

        if(variable_get('data_queue_debug_mode', FALSE))
          watchdog('data_queue', 'Inserting rows from @from to @to in rows queue.', array('@from'=>$from_row, '@to'=>$to_row), WATCHDOG_INFO);

        for ($i=$from_row; $i < $to_row; $i++) {
          $row_counter++;

          if(variable_get('data_queue_debug_mode', FALSE))
            watchdog('data_queue', 'Insert row number @number in rows queue.', array('@number'=>$row_counter), WATCHDOG_INFO);

          $row = $fieldata['data'][$i];
          // Process and validate row for queue.
          $process_row_for_queue = dq_insert_row_in_rows_queue($node->nid, $row, $row_counter, $node->dqf_processed_times[LANGUAGE_NONE][0]['value'], $by_uid, $rowsQueue);
          if($process_row_for_queue){
            $counted_rows++;

            /*
            // @TODO: Here we might delete and create queue item for file.
            // And update the queued rows one by one.

            // We will delete this and create it again with the new details.
            $importRowsQueue->deleteItem($item);
            // Create new item in queue.
            $new_item = $item->data;
            $new_item['queued_rows'] = $new_queued_rows;
            $importRowsQueue->createItem($new_item);
            */
          }

        }
      }else{
        if(variable_get('data_queue_debug_mode', FALSE))
          watchdog('data_queue', 'Inserting all file rows count: @count in rows queue.', array('@count'=>count($fieldata['data'])), WATCHDOG_INFO);
        $row_counter = 0;
        // Loop for all rows.
        foreach($fieldata['data'] as $row){
          $row_counter++;
          // Process and validate row for queue.
          $process_row_for_queue = dq_insert_row_in_rows_queue($node->nid, $row, $row_counter, $node->dqf_processed_times[LANGUAGE_NONE][0]['value'], $by_uid, $rowsQueue);
          if($process_row_for_queue){
            $row_counter++;
            $counted_rows++;
          }
        }
      }

      return TRUE;
    }else{
      if(variable_get('data_queue_debug_mode', FALSE))
        watchdog('data_queue', 'Error inserting file rows in rows queue.', array(), WATCHDOG_INFO);
      // ERROR.
      // Save log details.
      $theLog = array();
      $theLog['type'] = 'data_queue_file';
      $theLog['operation'] = 'fail';
      $theLog['form_id'] = 'dq_rows_prepare';
      $theLog['description'] = t('Can not read xlsx file of node:@nid (@message).', array('@nid'=>$node->nid, '@message'=>$fieldata['message']));
      $theLog['ref_char'] = 'node';
      $theLog['ref_numeric'] = $node->nid;
      $theLog['info'] = '';
      event_log_insert($theLog);
      return FALSE;
    }
  }else{
    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue', 'Trying to process node without file.', array(), WATCHDOG_INFO);
    // Save log details.
    $theLog = array();
    $theLog['type'] = 'data_queue_file';
    $theLog['operation'] = 'notice';
    $theLog['form_id'] = 'dq_rows_prepare';
    $theLog['description'] = t('No file in node:@nid.', array('@nid'=>$node->nid));
    $theLog['ref_char'] = 'node';
    $theLog['ref_numeric'] = $node->nid;
    $theLog['info'] = '';
    event_log_insert($theLog);
    return FALSE;
  }
}

/**
 * Insert row in rows queue.
 *
 * @param     array Row data.
 * @param     integer Row number for counter.
 * @return    boolean True Or False.
 */
function dq_insert_row_in_rows_queue($nid, $row, $row_number, $run_number = 1, $by_uid = FALSE, $queue){

  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue_rows', 'Start inserting row number @number in rows queue. ('.print_r($row).')', array('@number'=>$row_number), WATCHDOG_INFO);

  $validation_result = dq_validate_row_data('save', $row);
  // If this row is not valid we will continue to next row.
  if(!$validation_result['pass']){
    $file_data_node = node_load($nid);
    // Save log details.
    $theLog = array();
    $theLog['type'] = 'data_queue_file_row';
    $theLog['operation'] = 'fail';
    $theLog['form_id'] = 'dq_rows_worker_'.$run_number;
    $theLog['description'] = t('Row #@row from !file failed due validation errors.', array('@row'=>$row_number, '!file'=>l($file_data_node->title, 'node/'.$nid) ));
    $theLog['ref_char'] = 'node';
    $theLog['ref_numeric'] = $nid;
    if( isset($by_uid) && is_numeric($by_uid) )
      $theLog['uid'] = $by_uid; // Log this by the action tacker.
    else
      $theLog['uid'] = 2; // 2 = importer user.
    $theLog['info'] = array('row'=>$row, 'message'=>$validation_result['message']);
    event_log_insert($theLog);

    $row_number--;
    return FALSE;
  }

  $item = array('nid'=>$nid, 'row'=>$row, 'row_number'=>$row_number, 'run_number'=>$run_number);

  // If it is requested to save each row by the action tacker we will log user id.
  // This usually used if the user run the file the second time or more.
  if($by_uid)
    $item['by'] = $by_uid;

  if($queue->createItem($item)){
    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue_rows', 'Success inserting row number @number in rows queue. ('.print_r($item).')', array('@number'=>$row_number), WATCHDOG_INFO);
    return TRUE;
  }
  else{
    if(variable_get('data_queue_debug_mode', FALSE))
      watchdog('data_queue_rows', 'Can not insert valid row number @number in rows queue because DrupalQueue can not create items. ('.print_r($item).')', array('@number'=>$row_number), WATCHDOG_INFO);
    return FALSE;
  }
}

/**
 * Run process.
 */
function dq_rows_run(){
  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue', 'Running rows worker');

  $rowsQueue = DrupalQueue::get('dq_rows');

  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue', 'Found @number rows to be processed.', array('@number'=>$rowsQueue->numberOfItems()));

  $countThisProcess = 0;
  $thisProcessTotal = variable_get('dq_rows_queue_process_rows_each_time', 5);
  while ($item = $rowsQueue->claimItem(0)) {
    $countThisProcess++;
    if(dq_rows_worker($item)){
      // This item has been processed.
      $rowsQueue->deleteItem($item);
      watchdog('data_queue', t('Row number @number has been processed successfully for item @item_id for nid: @nid', array('@item_id'=>$item->item_id, '@nid'=>$item->data['nid'], '@number'=>$item->data['row_number'])), array(), WATCHDOG_INFO);
    }else{
      // Here we will delete this item and create it again.
      // So, this item will be at the end of current queue.
      $rowsQueue->deleteItem($item);
      $rowsQueue->createItem($item->data);
      watchdog('data_queue', t('Fail to process row number @number for item @item_id for nid: @nid', array('@item_id'=>$item->item_id, '@nid'=>$item->data['nid'], '@number'=>$item->data['row_number'])), array(), WATCHDOG_WARNING);
    }
    if($countThisProcess == $thisProcessTotal){
      break;
    }
  }
}

/**
 * Worker to process import and save records in database.
 */
function dq_rows_worker($item) {
  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue', 'worker is working on process row number @number: '.print_r($item, TRUE).'', array('@number'=>$item->data['row_number']));

  // Prepare queue item data.
  $file_data_nid = $item->data['nid'];
  $file_data_node = node_load($file_data_nid);
  $row = $item->data['row'];

  // Check validate row data.
  $validation_result = dq_validate_row_data('process', $row);
  if($validation_result['pass'] == TRUE){

  }else{
    // Save log details.
    $theLog = array();
    $theLog['type'] = 'data_queue_file_row';
    $theLog['operation'] = 'fail';
    $theLog['form_id'] = 'dq_rows_worker_'.$item->data['run_number'];
    $theLog['description'] = t('Row #@row from !file failed due validation errors.', array('@row'=>$item->data['row_number'], '!file'=>l($file_data_node->title, 'node/'.$file_data_nid) ));
    $theLog['ref_char'] = 'node';
    $theLog['ref_numeric'] = $file_data_nid;
    if( isset($item->data['by']) && is_numeric($item->data['by']) )
      $theLog['uid'] = $item->data['by']; // Log this by the action tacker.
    else
      $theLog['uid'] = 2; // 2 = importer user.
    $theLog['info'] = array('row'=>$row, 'message'=>$validation_result['message']);
    event_log_insert($theLog);

    // Abort saving this row to delete queue item to avoid stuck.
    return FALSE;
  }

  $process_status = TRUE;
  // $row_proccessing = module_invoke_all('data_queue_process_row_data', $row, $file_data_node);
  // Give other modules a chance to process row and alter each other results.
  foreach (module_implements('data_queue_process_row_data') as $module) {
    $function = $module . '_data_queue_process_row_data';
    if (function_exists($function)) {
      // Will call all modules implementing this hook.
      // And pass arguments from previous implement.
      $row_proccessing = $function($row, $file_data_node);

      // Only update status if the previous status is not equal FALSE.
      // Because if it is FALSE we want to return FALSE at the end of this function.
      if($process_status != FALSE)
        $process_status = $row_proccessing['pass'];
    }
  }

  if($row_proccessing['pass'] == TRUE){
    // Save log details.
    $theLog = array();
    $theLog['type'] = 'data_queue_file_row';
    $theLog['operation'] = 'info';
    $theLog['form_id'] = 'dq_rows_worker_'.$item->data['run_number'];
    $theLog['description'] = t('Row #@row from !file passed validation and processed successfully.', array('@row'=>$item->data['row_number'], '!file'=>l($file_data_node->title, 'node/'.$file_data_nid) ));
    $theLog['ref_char'] = 'node';
    $theLog['ref_numeric'] = $file_data_nid;
    if( isset($item->data['by']) && is_numeric($item->data['by']) )
      $theLog['uid'] = $item->data['by']; // Log this by the action tacker.
    else
      $theLog['uid'] = 2; // 2 = importer user.
    $theLog['info'] = array('row'=>$row, 'message'=>$validation_result['message']);
    event_log_insert($theLog);
  }else{
    // Save log details.
    $theLog = array();
    $theLog['type'] = 'data_queue_file_row';
    $theLog['operation'] = 'fail';
    $theLog['form_id'] = 'dq_rows_worker_'.$item->data['run_number'];
    $theLog['description'] = t('Row #@row from !file passed validation but failed to process.', array('@row'=>$item->data['row_number'], '!file'=>l($file_data_node->title, 'node/'.$file_data_nid) ));
    $theLog['ref_char'] = 'node';
    $theLog['ref_numeric'] = $file_data_nid;
    if( isset($item->data['by']) && is_numeric($item->data['by']) )
      $theLog['uid'] = $item->data['by']; // Log this by the action tacker.
    else
      $theLog['uid'] = 2; // 2 = importer user.
    $theLog['info'] = array('row'=>$row, 'message'=>'Fail to process');
    event_log_insert($theLog);
  }

  if(variable_get('data_queue_debug_mode', FALSE))
    watchdog('data_queue', 'Processing row number @number proccessed: '.print_r($item, TRUE).'', array('@number'=>$item->data['row_number']), WATCHDOG_INFO);

  return $row_proccessing['pass'];
}
