<?php

/**
 * @file
 * Administration pages for Data Queue cron.
 */


 /**
  * The form to provide a link to cron.php.
  */
 function dq_cron_job_form($form, &$form_state) {
   $form['status'] = array(
     '#type' => 'fieldset',
     '#title' => t('Cron status information'),
   );
   $form['status']['intro'] = array(
     '#markup' => '<div>' . t('You can run cron from this page and see the results.') . '</div>',
   );
   $form['status']['last'] = array(
     '#markup' => '<div>' . t('Next execution time will be after %time (%seconds seconds from now)',
       array(
         '%time' => date_iso8601(variable_get('data_queue_cron_next_execution', time())),
         '%seconds' => variable_get('data_queue_cron_next_execution', time()) - time(),
       )
     ) . '</div>',
   );

   $form['cron_run'] = array(
     '#type' => 'fieldset',
     '#title' => t('Run cron manually'),
   );

   $form['cron_run']['cron_reset'] = array(
     '#type' => 'checkbox',
     '#title' => t('Run cron regardless of whether interval has expired.'),
     '#default_value' => FALSE,
   );
   $form['cron_run']['cron_trigger'] = array(
     '#type' => 'submit',
     '#value' => t('Run cron now'),
     '#submit' => array('dq_form_cron_run_submit'),
     //'#submit' => array('cron_example_form_cron_run_submit'),
   );

   $form['cron_queue_setup'] = array(
     '#type' => 'fieldset',
     '#title' => t('Cron queue setup'),
   );

   $importFilesQueue = DrupalQueue::get('dq_files');
   $form['cron_queue_setup']['dq_files_queue_process_files_each_time'] = array(
     '#type' => 'select',
     '#title' => t('Number of files to process in @name queue each time', array('@name'=>t('Files Queue'))),
     '#options' => drupal_map_assoc(array(1, 5, 10, 100, 1000)),
     '#default_value' => variable_get('dq_files_queue_process_files_each_time', 5),
     '#description' => t('There are currently <strong>@num</strong> items in <strong>@name</strong> queue.', array('@num'=>$importFilesQueue->numberOfItems(),
         '@name'=>t('Files Queue'))),
   );
   $form['cron_queue_setup']['dq_files_queue_insert_rows_each_time'] = array(
     '#type' => 'select',
     '#title' => t('Number of rows to insert in @name queue each time', array('@name'=>t('Import files'))),
     '#options' => drupal_map_assoc(array(1, 5, 10, 100, 1000)),
     '#default_value' => variable_get('dq_files_queue_insert_rows_each_time', 5),
     '#description' => '',
   );
   $rowsQueue = DrupalQueue::get('dq_rows');
   $form['cron_queue_setup']['dq_rows_queue_process_rows_each_time'] = array(
     '#type' => 'select',
     '#title' => t('Number of rows to process in @name queue each time', array('@name'=>t('Import file rows'))),
     '#options' => drupal_map_assoc(array(1, 5, 10, 100, 1000)),
     '#default_value' => variable_get('dq_rows_queue_process_rows_each_time', 5),
     '#description' => t('There are currently <strong>@num</strong> items in <strong>@name</strong> queue.', array('@num'=>$rowsQueue->numberOfItems(),
         '@name'=>t('Import file rows'))),
   );

   $form['cron_queue_setup']['hr2'] = array('#markup'=>'<HR>');

   $form['configuration'] = array(
     '#type' => 'fieldset',
     '#title' => t('Configuration of cron'),
   );
   $form['configuration']['data_queue_cron_interval'] = array(
     '#type' => 'select',
     '#title' => t('Cron interval'),
     '#description' => t('After this time the cron job will be able to run again.'),
     '#default_value' => variable_get('data_queue_cron_interval', 60 * 60),
     '#options' => array(
       60 => t('1 minute'),
       300 => t('5 minutes'),
       3600 => t('1 hour'),
       60 * 60 * 24 => t('1 day'),
     ),
   );

   return system_settings_form($form);
 }

 /**
  * Allow user to directly execute cron, optionally forcing it.
  */
 function dq_form_cron_run_submit($form, &$form_state) {
   if (!empty($form_state['values']['cron_reset'])) {
     variable_set('data_queue_cron_next_execution', time());
   }

   // We can call drupal_cron_run() to run all Drupal crons.
   // But we will just use our custom cron handler directly.
   // dq_cron_handler(TRUE);
   if (drupal_cron_run()) {
     drupal_set_message(t('Cron ran successfully.'));
   }
   else {
     drupal_set_message(t('Cron run failed.'), 'error');
   }
 }
