<?php

/**
 * @file
 * Make sure ultimate_cron is enabled.
 */

/**
 * Reading .xlsx file and return rows as array.
 *
 * @param     file_uri
 * @return    array
 */
function dq_read_xlsx_file($file_uri){
  // Check if the file uri is not empty.
  if (trim($file_uri) == ''){
    return array('status'=>FALSE, 'message'=>'File URI is empty.');
  }

  // Check if the file URI is valid.
  if(!file_valid_uri($file_uri)){
    return array('status'=>FALSE, 'message'=>'File URI not valid.');
  }

  // Check if the file is exist.
  $xlsx_file = drupal_realpath($file_uri);
  if(!file_exists($xlsx_file)){
    return array('status'=>FALSE, 'message'=>'File not exists.');
  }

  module_load_include('inc', 'phpexcel');
  $xlsx_file_path = drupal_realpath($file_uri);
  $xlsx_sheets = phpexcel_import($xlsx_file_path, FALSE, TRUE);
  $xlsx_rows = current($xlsx_sheets);
  return array('status'=>TRUE, 'message'=>'SUCCESS', 'data'=>$xlsx_rows);
}

/**
 * Simple validation to check if the row already has values or just empty.
 * will return false if the row has no values.
 *
 * @param     array Row data.
 * @return    boolean True Or False.
 */
function dq_validate_xlsx_row($row){

  // Fetch all columns.
  foreach($row as $col){

    // Check if this column has values.
    if(strlen($col) > 0)
      return TRUE;

  }

  return FALSE;

}

/**
 * Validate imported row data.
 *
 * @param     array
 * @return    boolean
 */
function dq_validate_row_data($op, $row){
  $check_PASS = TRUE;
  $check_PASS_array = array();

  $validation_status = TRUE;
  $validation_messages = array();
  // $validation_result = module_invoke_all('data_queue_validate_row_data', $op, $row);
  foreach (module_implements('data_queue_validate_row_data') as $module) {
    $function = $module . '_data_queue_validate_row_data';
    if (function_exists($function)) {
      // Will call all modules implementing this hook.
      // And pass arguments from previous implement.
      $validation_result = $function($op, $row);

      // Only update status if the previous status is not equal FALSE.
      // Because if it is FALSE we want to return FALSE at the end of this function.
      if($validation_status != FALSE){
        $validation_status = $validation_result['pass'];
        if( isset($validation_result['message']) )
          $validation_messages[$module] = $validation_result['message'];
        else
          $validation_messages[$module] = '';
      }
    }
  }

  if($validation_status){
    $check_status = 'SUCCESS';
  }else{
    $check_status = 'NOT_VALID';
  }

  $returnArray = array();
  $returnArray['pass'] = $validation_status;
  $returnArray['status'] = $check_status;
  $returnArray['message'] = $validation_messages;
  $returnArray['row'] = $validation_result['row'];
  return $returnArray;
}
