<?php

/**
 * @file
 * Administration functions for Data Queue module.
 */

/**
 * Main settings page.
 */
 function dq_main_settings(){

   $form['cron_settings'] = array(
     '#type' => 'fieldset',
     '#title' => t('Data queue'),
   );

   $form['cron_settings']['data_queue_status'] = array(
     '#type' => 'radios',
     '#title' => t('Enable data queue functionality'),
     '#default_value' => variable_get('data_queue_status', 0),
     '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
     '#required' => TRUE,
   );

   $form['cron_settings']['data_queue_debug_mode'] = array(
     '#type' => 'radios',
     '#title' => t('Enable debug mode'),
     '#description' => t('This will set watchdog messages in the process.'),
     '#default_value' => variable_get('data_queue_debug_mode', 0),
     '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
     '#required' => TRUE,
   );
   return system_settings_form($form);
 }

/**
 * Showing table of data_queue_file processing log.
 *
 * @param     object
 * @return    void
 */
function dq_file_log($node){
  // We will get all event_logs of this current file.
  // Add results.
  $table = array(
    '#theme' => 'table',
    '#header' => array(
      array('data' => t('Status'), 'field' => 'operation'),
    array('data' => t('Process number')/*, 'field' => 'form_id', 'sort' => 'desc'*/),
      array('data' => t('Description')),
      array('data' => t('By')),
      array('data' => t('Date'), 'field' => 'created', 'sort' => 'desc'),
    ),
    '#rows' => array(),
  );

  $query = db_select('event_log', 'c')
    ->fields('c')
    ->extend('TableSort')
    ->orderByHeader($table['#header'])
    ->extend('PagerDefault')
    ->limit(50);

  // Apply filters.
  $query->condition('type', 'data_queue_file_row');
  $query->condition('form_id', db_like('dq_rows_worker_') . '%', 'LIKE');
  $query->condition('ref_char', 'node');
  $query->condition('ref_numeric', $node->nid);
  $total = $query->countQuery()->execute()->fetchField();

  $result = $query->execute();

  $query = db_select('event_log', 'c')
    ->fields('c')
    ->condition('operation', 'info')
    ->condition('type', 'data_queue_file_row')
    ->condition('form_id', db_like('dq_rows_worker_') . '%', 'LIKE')
    ->condition('ref_char', 'node')
    ->condition('ref_numeric', $node->nid);
  $info = $query->countQuery()->execute()->fetchField();

  $query = db_select('event_log', 'c')
    ->fields('c')
    ->condition('operation', 'fail')
    ->condition('type', 'data_queue_file_row')
    ->condition('form_id', db_like('dq_rows_worker_') . '%', 'LIKE')
    ->condition('ref_char', 'node')
    ->condition('ref_numeric', $node->nid);
  $fail = $query->countQuery()->execute()->fetchField();

  $table['#caption'] = t('@info row success and @fail failed with total of @total rows found.', array('@info'=>$info, '@fail'=>$fail, '@total' => $total));

  foreach ($result as $record) {
    if (!empty($record->uid)) {
      $account = user_load($record->uid);
    }
    else {
      $account = NULL;
    }
    $process_times = str_replace('dq_rows_worker_', '', check_plain($record->form_id));
    $table['#rows'][] = array(
      array('data' => check_plain($record->operation)),
      array('data' => format_string('#@number', array('@number'=>$process_times) )),
      array('data' => $record->description),
      array('data' => (empty($account) ? '' : l($account->name, 'user/' . $account->uid))),
      array('data' => check_plain(date("Y-m-d H:i:s", $record->created))),
      // $record->ip
      // $record->ref_numeric
      // $record->ref_char
      // $record->info
    );
  }

  $render = array(
    'table' => $table,
    'pager' => array('#theme' => 'pager'),
  );

  return $render;
}

/**
 * Showing table of data_queue_file processing log.
 *
 * @param     object
 * @return    void
 */
function dq_file_rows($node){
  // We will get all event_logs of this current file.
  // Add results.
  $table = array(
    '#theme' => 'table',
    '#header' => array(
      array('data' => t('Row number'), 'field' => 'form_id'),
      array('data' => t('Process number')),
      array('data' => t('Status')),
      array('data' => t('Data')),
    ),
    '#rows' => array(),
  );

  // Query to get last process form_id.
  $query = db_select('event_log', 'e')
  ->distinct()
  ->fields('e', array('form_id'))
  ->groupBy('e.form_id')
  ->orderBy('e.form_id', 'DESC')
  ->condition('type', 'data_queue_file_row')
  ->condition('form_id', db_like('dq_rows_worker_') . '%', 'LIKE')
  ->condition('ref_char', 'node')
  ->condition('ref_numeric', $node->nid);
  $form_id = $query->execute()->fetchField();

  // Query to get results.
  $query = db_select('event_log', 'c')
    ->fields('c')
    ->extend('TableSort')
    ->orderByHeader($table['#header'])
    ->extend('PagerDefault')
    ->limit(50);

  // Apply filters.
  $query->condition('type', 'data_queue_file_row');
  $query->condition('form_id', $form_id, '=');
  //$query->condition('operation', 'info', '=');
  $query->condition('ref_char', 'node');
  $query->condition('ref_numeric', $node->nid);
  $total = $query->countQuery()->execute()->fetchField();
  $table['#caption'] = t('@total rows found.', array('@total' => $total));

  $result = $query->execute();
  $new_result = array();
  foreach ($result as $record) {
    preg_match_all('/#(\d+)/', $record->description, $row_number, PREG_SET_ORDER, 0);
    $row_number = $row_number[0][0];
    $new_result[$row_number] = $record;
  }

  foreach ($new_result as $record) {
    if (!empty($record->uid)) {
      $account = user_load($record->uid);
    }
    else {
      $account = NULL;
    }

    if($record->operation == 'info')
      $record->operation = 'success';

    $process_times = str_replace('dq_rows_worker_', '', check_plain($record->form_id));
    $the_info = drupal_json_decode($record->info);
    if( is_array($the_info['row']) ){
      $the_info_translated = array();
      // Translate info messages.
      foreach($the_info['row'] as $info_text){
        $the_info_translated[] = t($info_text);
      }
      $the_info = $the_info_translated;
    }
    $info_inline = implode($the_info, ', ');

    if($record->operation == 'fail'){
      $the_info = drupal_json_decode($record->info);
      if(isset($the_info['message'])){
        $info_inline = t($the_info['message']). '<br />'.implode($the_info['row'], ', ').'';
      }
    }

    preg_match_all('/#(\d+)/', $record->description, $row_number, PREG_SET_ORDER, 0);
    $row_number = $row_number[0][0];

    $ref_char = '';
    /*if( isset($the_info[0]) ){
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'beneficiary')
        ->propertyCondition('status', 1)
        ->fieldCondition('field_beneficiary_id', 'value', check_plain($the_info[0]), '=');
      $result = $query->execute();
      if( isset($result['node']) ){
        $nid = array_keys($result['node']);
        if( isset($nid[0]) ){
          $nid = $nid[0];
          $info_inline = l($info_inline, 'node/'.$nid);
        }
      }
    }*/

    if (($record->operation == 'fail') && is_string($the_info)) {
      $info_inline = t($record->info);
    }

    // Capitalize first chracter.
    $record->operation = ucfirst($record->operation);
    $process_times = str_replace('dq_rows_worker_', '', check_plain($record->form_id));
    $table['#rows'][] = array(
      array('data' => $row_number),
      array('data' => format_string('#@number', array('@number'=>$process_times) )),
      array('data' => t($record->operation)),
      array('data' => $info_inline),
    );
  }

  $render = array(
    'table' => $table,
    'pager' => array('#theme' => 'pager'),
  );

  return $render;
}
